var supertest=require('supertest');
var should=require('should');
var api=supertest('http://localhost:3000');

describe('directory list',()=>{
	

	it('returns a 200 for retrieved directories',(done)=>{
		api.get('/directories')
		.expect(200,done);
	});
});


describe('directory creation',()=>{
	it('returns 200 and created object',(done)=>{
		api.post('/directories')
		.send({email:'lisa@thesimpsons.com',phoneNumber:'01671143556'})
		.expect(200)
		.end(function(err, res) {
        if (err) done(err);
        res.body.should.have.property('email','lisa@thesimpsons.com');
        res.body.should.have.property('phoneNumber', '01671143556');
        done();
      });
	});

});


