var Directory=require('../models/directory.model');

exports.getDirectories=function(req,res){

	Directory.find().then(function(directories){
		
		if(!directories)
		{
			res.status(404).send("No directories to show");
		}
		else
		{
			res.status(200).json(directories);
		}

	}).
	catch(function(err){
		if(err) res.status(200).send('No directories available');
	});
};

exports.createDirectories=function(req,res){
    Directory.create(req.body).then(function(directory){
    	res.status(200).json(directory);
    })
    .catch(function(err){
    	res.status(500).send(err);
    })
};
