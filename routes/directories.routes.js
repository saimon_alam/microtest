var express=require('express'),
router=express.Router(),
DirectoryController=require('../controllers/directory.controller');

router.get('/',DirectoryController.getDirectories);

router.post('/',DirectoryController.createDirectories);

module.exports=router;
